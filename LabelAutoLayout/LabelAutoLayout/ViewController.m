//
//  ViewController.m
//  LabelAutoLayout
//
//  Created by kanzhun on 16/5/25.
//  Copyright © 2016年 kanzhun. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UILabel *leftLabel;
@property (weak, nonatomic) IBOutlet UILabel *rightLabel;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/**
 *  希望达到的效果
 *  leftLabel的内容全部显示
 *  rightLabel的长度内容不确定，若特别长则显示省略号；若不是特别长则完全显示
 *  以上功能已经实现，下边功能未实现
 *  现在希望红色的叹号始终与rightLabel保持10像素，与leftLabel最小保持10像素或大于10像素
 *
 */
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    self.leftLabel.text = @"leftLabelleftLabel";
    self.rightLabel.text = @"rightLabelrightLabelrightLabelrightLabelrightLabelrightLabel";
}

@end
