//
//  main.m
//  LabelAutoLayout
//
//  Created by kanzhun on 16/5/25.
//  Copyright © 2016年 kanzhun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
